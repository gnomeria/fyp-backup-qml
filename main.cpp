#include <QtGui/QApplication>
#include "qmlapplicationviewer.h"
#include "performancemeter.h"
#include "fullscreencontrol.h"
//#include <QMessageBox>
#include <QGraphicsEffect>


Q_DECL_EXPORT int main(int argc, char *argv[])
{
    bool FULL_SCREEN = false;
    const bool DEBUGGING = false;
//    QScopedPointer<QApplication> app(createApplication(argc, argv));
    QApplication app(argc, argv);
    app.setApplicationName ("FYP Project");
    QmlApplicationViewer viewer;


    //Register the 'benchmark' function and expose it to be used in qml
    qmlRegisterType<PerformanceMeter>("PerformanceAnalysis", 1, 0, "PerformanceMeter");
    qmlRegisterType<QGraphicsBlurEffect>("Effects",1,0,"Blur"); //Adding the blur effect support
    //Below are the screen/view initialization, included about will the view be displayed in fullscreen or not
    //it depends on FULL_SCREEN final variable is 'true' or 'false'
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/Latihan/main.qml"));
    viewer.showExpanded();
    viewer.setMinimumSize(QSize(800,480));
    viewer.setResizeMode( QDeclarativeView::SizeRootObjectToView );   
    if (FULL_SCREEN)
        viewer.showFullScreen ();

    viewer.rootObject ()->setProperty ("debugging", DEBUGGING);

    //Connect the signal that'll be emitted when the video is double-clicked
    QObject *contObject = ((viewer.rootObject())->findChild<QObject*>("container"));


    FullScreenControl fsc(&viewer);

    QObject::connect ( contObject, SIGNAL(fscreen()), &fsc, SLOT(videoDoubleClicked()) ); //The function of double-clicks will be connected to the fullscreen function of *viewer->showFullScreen()
//    QObject::connect ( &fsc, SIGNAL(fscreenClicked()), rootObject, SLOT(goFullScreen()) );

    return app.exec ();
}
