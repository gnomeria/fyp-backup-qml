// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt.labs.particles 1.0
import "./botbar/"
import "./Video"
Item {
    id:container

    signal vidState;
    signal inetState;
    signal fscreen(bool isFscreen);
    signal initDb;

    property string videoFile: "file:///home/sami/Documents/QtProject/Latihan/qml/Latihan/VideoView.qml";

    //these lat,long are of IIUM location
    property double lat: 3.2514618155407863;
    property double lon: 101.73498630523682;
    property int zoomLevel: 13;
    property bool showBotbar: false;
    signal showBotbarSig(string currentApp, bool showBotbar);
//    ButtonsMap{id:_bMap}

    function changeState(newState)
    {
        if (this.state!==newState)
        {
            this.state=newState;
        }
    }
    function playVideo()
    {
        if (pageLoader.source==videoFile)
            pageLoader.item.videoClicked();
        else
            console.log("Loooooooooossssssseeeeeeeeeeerrrrrrrrrrr !!!!!!!!!!!!!!!!!!!!");
    }

    Behavior on height
    {
        SmoothedAnimation {duration: 500}
    }

    onShowBotbarChanged: showBotbarSig(container.state, showBotbar);

    Loader{
        id: pageLoader
        smooth: true
        x: parent.x;
        width: parent.width; height: parent.height;
        source:"HomeView.qml"

        Behavior on source
        {
            ParallelAnimation
            {
                PropertyAnimation { target: pageLoader; property: "x"; easing.type: Easing.OutBack; to:parent.x; duration:500 }
                PropertyAnimation { target: pageLoader; property:"opacity"; easing.type: Easing.InQuint;duration:300; to:1 }
            }
        }
        Connections
        {
            target: pageLoader.item
            onFscreen:
            {
                console.log("Ooohhh lala !!!");
                container.fscreen();
            }
            onDbChanged: container.initDb();
        }
    }

    states: [
        State {
            name: "homeState";
            PropertyChanges{
                target: pageLoader;
                source: "HomeView.qml";
            }
        },
        State {
            name: "musicsState"
            PropertyChanges {
                target: pageLoader;
                source: "./musics/MusicsView.qml";
            }
            PropertyChanges {
                target: container
                showBotbar: true;
            }
        },
        State {
            name: "moviesState"
            PropertyChanges {
                target: pageLoader;
                source: "./Video/VideoView.qml";
            }
            PropertyChanges {
                target: container
                showBotbar: true;
            }
        },
        State {
            name: "internetState"
            PropertyChanges {
                target: pageLoader;
                source: "./Browser/BrowserMainView.qml";
            }
            PropertyChanges {
                target: container
                showBotbar: true;
            }
        },
        State {
            name: "mapsState"
            PropertyChanges {
                target: pageLoader;
                source: "./Navigation/MapView.qml";
            }
            onCompleted: {
                pageLoader.item.setLocation(lat,lon);
                pageLoader.item.setMapZoom(zoomLevel);
                setBotbarModel(_bMap);
            }
            PropertyChanges {
                target: container
                showBotbar: true;
            }
        },
        State {
            name: "settingsState"
            PropertyChanges {
                target: pageLoader
                source: "./Settings/SettingsView2.qml"
            }
            PropertyChanges {
                target: container
                showBotbar: false;
            }
        }
    ]
    transitions: Transition
    {
    SequentialAnimation
    {
        ParallelAnimation
        {
            PropertyAnimation { target: pageLoader; property:"x"; easing.type: Easing.OutCubic; to:-parent.width; duration:500 }
            PropertyAnimation { target: pageLoader; property:"opacity"; easing.type: Easing.Linear; to:0.5; duration:300}
            NumberAnimation { target: pageLoader; property: "source"; duration: 10; easing.type: Easing.InOutQuad }
        }
        PropertyAction { target: pageLoader; property: "x"; value: container.width; }
    }
    reversible:true
}

//------------------------------------------------------------- HANDLING TOOLBAR ACTIONS --------------------------------------------------------
function handleToolbarEvent(event) {
        pageLoader.item.handleToolbarEvent(event);
    }
}

