import QtQuick 1.0
//import "../botbar"

// Toolbar buttons for the Browser application
ListModel {
    ListElement {
        buttonText:"BACK"
        event: "back"
        iconImage: "./icons/icon_back.png"
    }
    ListElement {
        buttonText: "FORWARD"
        event: "forward"
        iconImage: "./icons/icon_forward.png"
    }
    ListElement {
        buttonText: "RELOAD"
        event: "reload"
        iconImage: "./icons/icon_refresh.png"

    }
    ListElement {
        buttonText: "URL"
        event: "enterurl"
        iconImage: "./icons/icon_url.png"
    }
    ListElement {
        buttonText: "BOOKMARKS"
        event: "listview"
        iconImage: "./icons/icon_bookmarks_list.png"
    }
    ListElement {
	buttonText: ""
        event: "gridview"
        iconImage: "./icon_bookmarks_thumbs.png"
    }
//    ListElement {
//        buttonText:"FULLSCREEN"
//        event: "fullscreen"
//        iconImage: "./images/icon_fullscreen.png"
//        blink: false
//        buttonEnabled: true
//        shareButtonText: ""
//    }
    ListElement {
	buttonText: "ZOOM"
        event: "zoom_out"
        iconImage: "./icons/icon_zoom_out.png"
    }
    ListElement {
	buttonText: ""
        event: "zoom_in"
        iconImage: "./icons/icon_zoom_in.png"
    }
}
