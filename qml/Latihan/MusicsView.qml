import QtQuick 1.1
import QtMultimediaKit 1.1

//Item
//{
//    id: page
//    property bool playing: false;


//    anchors.fill: parent;
//    Rectangle
//    {
//        width: 200;height:100;
//        anchors.horizontalCenter: parent.horizontalCenter
//        anchors.verticalCenter: parent.verticalCenter
//        color:"white"

//        Text{
//            id: text
//            anchors.horizontalCenter: parent.horizontalCenter
//            anchors.verticalCenter: parent.verticalCenter
//            text: "Click to play";
//            font.pointSize: 20;
//            color: "black"
//        }
//        MouseArea
//        {
//            anchors.fill: parent
//            id:maPlay
//            Audio{
//                id: music
//                source: "music.mp3"
//            }
//            onClicked: {
//                if (playing = !playing)
//                {
//                    text.text = "Click to pause";
//                    music.play();
//                }
//                else
//                {
//                    text.text = "Click to play";
//                    music.pause();
//                }
//            }
//        }
//    }
//}

//Item
//{
Flipable
{
    id:musicContainer
    ListView {
        id: listView
        anchors.fill: parent;
//    property bool playing: false;
        delegate: Item {
            x: 5
            height: 40
            Row {
                id: row1
                spacing: 10
                Rectangle {
                    width: 40
                    height: 40
                    color: colorCode
                }

                Text {
                    text: name
                    anchors.verticalCenter: parent.verticalCenter
                    font.bold: true
                }
            }
        }
        model: ListModel {
            ListElement {
                name: "Grey"
                colorCode: "grey"
            }

            ListElement {
                name: "Red"
                colorCode: "red"
            }

            ListElement {
                name: "Blue"
                colorCode: "blue"
            }

            ListElement {
                name: "Green"
                colorCode: "green"
            }
            ListElement {
                name: "Black"
                colorCode: "black"
            }
            ListElement {
                name: "Purple"
                colorCode: "purple"
            }
            ListElement {
                name: "White"
                colorCode: "white"
            }
            ListElement {
                name: "Aqua"
                colorCode: "aqua"
            }
            ListElement {
                name: "Pink"
                colorCode: "pink"
            }
        }
    }
}
