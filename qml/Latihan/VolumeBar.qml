// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Slider {
     orientation: Qt.Vertical
     maximumValue: 25
     stepSize: 1
     value: 25
     valueIndicatorVisible: true
     valueIndicatorText: "Volume"
     inverted: true
 }
