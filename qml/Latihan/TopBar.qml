// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
//import Qt.labs.particles 1.0
Rectangle {
    id:topbar
    height: 100
    width: parent.width
//    anchors.top: parent.top
//    anchors.left: parent.left
    color:"transparent"
    //color:"#213873"

    Row {
        id: row1
        spacing:((parent.width-600)/6)
        smooth: true
        anchors.leftMargin: 30
        anchors.fill: parent
        MyEffects{
                 id:eff
               }
        HomeIcons{ id: _maHome; iName: "home"}
        HomeIcons{ id: _maMusics; iName: "musics" }
        HomeIcons{ id: _maMovies; iName: "movies" }
        HomeIcons{ id: _maBrowser; iName: "internet" }
        HomeIcons{ id: _maMaps; iName: "maps" }
        HomeIcons{ id: _maSettings; iName: "settings" }
    }
}
