// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

ListModel{
    ListElement{
        buttonText:"ZOOM OUT"
        event: "zoom_out"
        iconImage: "./images/icon_zoom_out.png"
    }
    ListElement{
        buttonText:"ZOOM IN"
        event: "zoom_in"
        iconImage: "./images/icon_zoom_in.png"
    }
}
