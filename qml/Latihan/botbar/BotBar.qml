// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id:botbar
    height: 62
    width: parent.width
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    //    color:"#213873";
    color: "transparent"
    //    property bool toPlay: false;
    signal toPlay;
    signal fullscreen;
    //    ScriptAction{script:console.log(botbar.width)}
    MusicControl{
        id:mControl;
        anchors.horizontalCenter: parent.horizontalCenter;
        onPlayClicked: toPlay();
    }
    MouseArea{
        id: maVolume
        width: 64
        height: parent.height
        anchors.left: parent.left
        anchors.leftMargin: 30
        signal volClicked;
        Image{
            id: volumeIcon
            anchors.fill: parent;
            source: "./icons/sound_on_3.png"
        }

        onClicked: volClicked;
//        onPressed: console.log("Volume button area being pressed !!!")
//        onReleased: console.log("Mouse click released !!!")

    }
    states: [
        State {
            name: "visible"
        },
        State {
            name: "hidden"
            PropertyChanges {}
        }
    ]

//    Behavior on visible{
//        ParallelAnimation{
//            PropertyAnimation{property:"opacity"; to:0; duration:1000}
//            NumberAnimation {duration: 1000; easing.type: Easing.InOutQuad }
//        }
//    }

}
