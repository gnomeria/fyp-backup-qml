// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../Browser/"

Item {
    id: botbar;
//    width: parent.width
    height: 62
    signal btnClicked(string event);
    //Delegate component model


    ListView{
        id: buttons
        height: parent.height
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        model: ButtonsDataBrowser{}
        delegate: BotBarDelegate{}
        orientation: ListView.Horizontal
        interactive: false
        spacing: 10
    }
}
