// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Component
{
    id: btnDrawer
    MouseArea{
        height: parent.height
        width: 62
            Image{
                id:icon;
                source: iconImage;
                sourceSize.height: 35
                sourceSize.width: 35
                fillMode: Image.Stretch;
            }
            Text{
                id: iconLabel
                anchors.top: icon.bottom
                anchors.topMargin: 5
                text: buttonText
                font.pixelSize: 12
                color: "black"
            }

            onClicked: btnClicked(event);
        }
    }
