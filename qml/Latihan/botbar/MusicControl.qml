import QtQuick 1.1

Item{
    id: musicControl
    height:62
    width:190

    property real itemWidth : 62
    property bool playing: false

    property string iPlaySource: "icons/play.png"
    property string iPauseSource: "icons/pause.png"
    property string iTrackNextSource: "icons/track[next].png"
    property string iTrackPrevSource: "icons/track[prev].png"

    signal playClicked


    MouseArea {
        id: maPrevTrack
        anchors.left: parent.left
        width: itemWidth
        height: musicControl.height
        Image {
            id: prevTrackIcon
            width: parent.width
            height: parent.height
            smooth: true
            source: iTrackPrevSource
        }
        onClicked:{console.log("Prev button clicked !!! ;)")}
    }

    MouseArea
    {
        id: maPlay
        width: itemWidth
        height: musicControl.height
        anchors.horizontalCenter: musicControl.horizontalCenter
        Image {
            id: playIcon
            width: parent.width
            height: parent.height
            smooth:true;
            source: iPlaySource
        }
        onClicked:{
//            console.log("Play button clicked !!! ;)\nSource : "+ playIcon.source);
            playClicked();
            if (playing==false)
            {
                playIcon.source=iPauseSource;
                playing=true;
            }
            else
            {
                playIcon.source=iPlaySource;
                playing=false;
            }
        }
    }

    MouseArea {
        id: maNextTrack
        anchors.right: parent.right
        width: itemWidth
        height: musicControl.height
        Image {
            id: nextTrackIcon
            width: parent.width
            height: parent.height
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: iTrackNextSource
        }
        onClicked:{console.log("Next button clicked !!! ;)")}
    }

}
