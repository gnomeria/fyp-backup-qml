import QtQuick 1.1
import "../"
Item{

//    MyEffects{
//        id: bgEffects
//        anchors.fill: parent;
//        count:30
//        emissionRate: 15
//        emissionVariance: 50
//        opacity: 0.5
//        lifeSpan: 3000
//        lifeSpanDeviation: 6000
//        source: "../effects/white.png";
//        xvar: 5
//        yvar: 90
//        velocity: 20
//    }
//    Rectangle{
//        id: background
//        gradient: Gradient {
//            GradientStop {
//                position: 0
//                color: "#cdc7f5"
//            }

//            GradientStop {
//                position: 1
//                color: "#48536d"
//            }
//        }
//        anchors.fill: parent
//        opacity: 0.3
//    }

    VideoPlayer{id:videoPlayer; anchors.fill: parent; source: fileSource; scale:1}
    Rectangle{
        id: progressBar
        opacity: 0.5
        anchors.bottom: parent.bottom
        property double fitToWidth: ( videoContainer.width / (videoContainer.duration/500) ); //this is to dynamically map the value of remaining time to the width of the app in pixels
        height:32
        radius: 1
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#191742"
            }

            GradientStop {
                position: 1
                color: "#4d7ee8"
            }
        }
        width: ( (videoPlayer.position/500) * fitToWidth )
        Behavior on width {
            PropertyAnimation{duration:500}
        }
    }
    Rectangle{
        id: volumeBar
        property double fitToHeight: parent.height/100
        height: parent.height
        radius: 1
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#00ffffff"
            }

            GradientStop {
                position: 1-videoPlayer.volume
                color: "#6ea7f5"
            }

            GradientStop {
                position: 1
                color: "#006ea7f5"
            }
        }
//        height: fitToHeight*2*(videoPlayer.volume*100);
//        height: parent.height
        width: 32
        anchors.right: parent.right
    }
    MouseArea{
        anchors.fill: parent
        onClicked: videoPlayer.play();
//        onClicked: videoContainer.flipped = !videoContainer.flipped;
    }
}
