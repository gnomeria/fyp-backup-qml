import QtQuick 1.1
import QtMultimediaKit 1.1

Video{
//    id: videoPlayer
//    source: "../../../Videos/video.mp4"
    property int elapsed;
    property string remainingTime;
    muted: false;
    onBufferProgressChanged: trackProgress();
    onSourceChanged: {
        elapsed=0; //reset the elapsed time if different audio is playing
        console.log(videoPlayer.metaData.audioBitRate)
    }
    onError: console.log(videoPlayer.errorString);
    fillMode: Video.PreserveAspectFit
//------------------------- << Functions to handle track progress >> ------------------------
    function trackProgress()
    {
        elapsed += ((videoPlayer.position%1000) > 500) ? 1 : 0;

        if ( ((videoPlayer.position % 1000) > 500) )
        {
            remainingTime = remainingTimeString(videoPlayer.position/1000, videoPlayer.duration);
            console.log("elapsed : " + elapsed + ", remaining : " + remainingTime);
        }
    }
    function remainingTimeString(elapsed, _duration)
    {
        var remaining = Math.ceil((_duration/1000)-elapsed);
        var minute=0, seconds = Math.floor((duration-elapsed)%60);
        if (debug)
            console.log(remaining);
        minute = Math.floor(remaining/60);
        return minute+"m"+seconds+"s";
    }
}//end of video element

