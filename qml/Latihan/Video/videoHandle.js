//----------------------------------- << Handling the bottom bar events >> ---------------------------------
    function handleToolbarEvent(event)
    {
        switch(event)
        {
            case "back":
                musicContainer.state="back";
                musicContainer.flipped=!musicContainer.flipped;
                break;
            case "forward":
                audioPlayer.muted = !audioPlayer.muted;
                console.log("The player is now " + audioPlayer.muted ? "" : "un-" + "muted");
                break;
            case "zoom_out":
                audioPlayer.volume = audioPlayer.volume>0.0 ? audioPlayer.volume-0.1 : 0;
                console.log("Volume down bebeh !!!");
                break;
            case "zoom_in":
                audioPlayer.volume = audioPlayer.volume<1.0 ? audioPlayer.volume+0.1 : 1.0;
                console.log("Volume up bebeh !!!");
                break;
        }
    }
