import QtQuick 1.1
import Qt.labs.folderlistmodel 1.0

FolderListModel{
    id: videoModel
    folder: filePrefix
    nameFilters: ["*.mp4", "*.avi"];
    showDirs: false;
}
