import QtQuick 1.1

ListView {
    id: listView
    boundsBehavior: Flickable.DragAndOvershootBounds
    snapMode: ListView.SnapToItem
    spacing: 5
    anchors.fill: parent;
    delegate: VideoDelegate{}
    model: VideoModel{}
    scale: 1
}
