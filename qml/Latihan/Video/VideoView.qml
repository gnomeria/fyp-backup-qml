// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import QtMultimediaKit 1.1
import "../"

Flipable{
    id: videoContainer
    property bool flipped: false;
    property bool debug: false;
    property string filePrefix: "../../../MediaFiles/Videos/"
    property string fileSource: ""
    property string filePlaying: ""

    front: listView
    back: screenView

//    VideoPlayer{id: videoPlayer}
    VideoListView{id: listView}
    VideoScreenView{id: screenView; anchors.fill: videoContainer; scale:0}


//------------------------------------------- < Flip functions > -----------------------------------------
    //Flip animation property changes
    states: [
        State {
            name: "back"
            PropertyChanges {
                target: rotation
                angle: 180
            }
            PropertyChanges {
                target: screenView
                opacity: 1
                scale: 1
            }
            PropertyChanges {
                target: listView
                scale: 0
            }
            when: videoContainer.flipped;
        }
    ]
//------------------------------------------ < Animations definitions >-------------------------------------
    transform: Rotation {
             id: rotation
             origin.x: videoContainer.width/2
             origin.y: videoContainer.height
             axis.x: 0; axis.y: 1; axis.z: 0     // set axis.y to 1 to rotate around y-axis
         }

    transitions: Transition {
        PropertyAnimation {target: listView; property:"scale"; duration: 500}
        PropertyAnimation {target: screenView; property: "opacity"; duration: 300; easing.type: Easing.InCirc}
        PropertyAnimation {target: screenView; property: "scale"; duration: 500}
        SmoothedAnimation {target: rotation; property: "angle"; easing.type: Easing.InOutQuad }
    }

//----------------------------------- << Handling the bottom bar events >> ---------------------------------
        function handleToolbarEvent(event)
        {
            var videoPlayer = screenView.children[0];
            switch(event)
            {
                case "back":
                    videoContainer.state="back";
                    videoContainer.flipped=!videoContainer.flipped;
                    break;
                case "forward":
                    videoPlayer.muted = !videoPlayer.muted;
                    console.log("The player is now " + videoPlayer.muted ? "" : "un-" + "muted");
                    break;
                case "zoom_out":
                    videoPlayer.volume = videoPlayer.volume>0.0 ? videoPlayer.volume-0.1 : 0;
                    console.log("Volume down bebeh !!!");
                    break;
                case "zoom_in":
                    videoPlayer.volume = videoPlayer.volume<1.0 ? videoPlayer.volume+0.1 : 1.0;
                    console.log("Volume up bebeh !!!");
                    break;
            }
        }

}
