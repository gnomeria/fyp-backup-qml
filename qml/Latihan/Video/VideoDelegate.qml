import QtQuick 1.1

Component{
    Item {
        id: drawer
        property bool isPlaying: false;
        x: 5
        height: 40
        width: parent.width
        Row {
            spacing: 10
            MouseArea   //To handle list items clicked
            {
                height: drawer.height
                width: parent.parent.width
                Rectangle {     //Background of each item to differentiate from each other
                    id: contextContainer
                    anchors.fill: parent
                    radius: 7
                    gradient: Gradient {
                        GradientStop {
                            position: 0
                            color: "#6782b1"
                        }

                        GradientStop {
                            position: 0.530
                            color: "#00000000"
                        }
                    }
                    border.width: 0
                    border.color: "#000000"
                    Text {
                        text: fileName;
                        anchors.verticalCenter: parent.verticalCenter
                        font.bold: true
                        color: "white"
                    }
                }
                onClicked:
                {
                    listView.currentIndex = index;
                    mFileClicked();
                    videoContainer.flipped = !videoContainer.flipped;
                }
            }
        }
        function mFileClicked()
        {
            if (!isPlaying)
            {
                videoContainer.filePlaying = fileName;
                videoContainer.fileSource = filePrefix + videoContainer.filePlaying;
//                videoPlayer.source = filePrefix + videoContainer.filePlaying;
//                videoPlayer.play();
            }
//            else
//                VideoScreenView

            drawer.isPlaying = !drawer.isPlaying;
        }
    }

}
