// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt.labs.folderlistmodel 1.0

FolderListModel{
    id: musicModel
    folder: "../../../MediaFiles/Musics"
    nameFilters: ["*.mp3"];
    showDirs: false;
}

//ListModel {
//    ListElement {
//        artistName  : "Andy McKee"
//        songName    : "Art Of Motion"
//    }
//    ListElement {
//        artistName  : "Andy McKee"
//        songName    : "Art ofMotion 2"
//    }
//}
