import QtQuick 1.1
import QtMultimediaKit 1.1

Flipable
{
    id: musicContainer
    front: musicListView
    back: musicDetailView
    property bool flipped: false;
    property bool debug: false;
    property string filePlaying: ""

    Audio{
        id: audioPlayer
        property int elapsed;
        property string remainingTime;
        muted: false;
        onBufferProgressChanged: trackProgress();
        onSourceChanged: {
            elapsed=0; //reset the elapsed time if different audio is playing
            console.log(audioPlayer.metaData.audioBitRate)
        }
        onError: console.log(audioPlayer.errorString);

//------------------------- << Functions to handle track progress >> ------------------------
        function trackProgress()
        {
            elapsed += ((audioPlayer.position%1000) > 500) ? 1 : 0;

            if ( ((audioPlayer.position % 1000) > 500) )
            {
                remainingTime = remainingTimeString(audioPlayer.position/1000, audioPlayer.duration);
                console.log("elapsed : " + elapsed + ", remaining : " + remainingTime);
            }
        }
        function remainingTimeString(elapsed, _duration)
        {
            var remaining = Math.ceil((_duration/1000)-elapsed);
            var minute=0, seconds = Math.floor((duration-elapsed)%60);
            if (debug)
                console.log(remaining);
            minute = Math.floor(remaining/60);
            return minute+"m"+seconds+"s";
        }
    }//end of audio element
    MusicsListView{id: listView}
    MusicsDetailView{id:musicDetailView; scale:0}



    transform: Rotation {
             id: rotation
             origin.x: musicContainer.width/2
             origin.y: musicContainer.height
             axis.x: 0; axis.y: 1; axis.z: 0     // set axis.y to 1 to rotate around y-axis
//             angle: 0    // the default angle
         }
    Scale {
        id: scaling
        origin.x: musicContainer.width/2
        origin.y: musicContainer.height
        xScale: 1
        yScale: 1
    }

    states:
        State{
            name: "back"
            PropertyChanges {
                target: rotation
                angle: 180
            }
            PropertyChanges {
                target: musicDetailView
                opacity: 1
                scale: 1
            }
            PropertyChanges {
                target: listView
                scale: 0
            }
            when: musicContainer.flipped;
    }

    transitions: Transition {
        PropertyAnimation {target: listView; property:"scale"; duration: 500}
        PropertyAnimation {target: musicDetailView; property: "opacity"; duration: 300; easing.type: Easing.InCirc}
        PropertyAnimation {target: musicDetailView; property: "scale"; duration: 500}
        SmoothedAnimation {target: rotation; property: "angle"; easing.type: Easing.InOutQuad }
    }
//----------------------------------- << Handling the bottom bar events >> ---------------------------------
    function handleToolbarEvent(event)
    {
        switch(event)
        {
            case "back":
                musicContainer.state="back";
                musicContainer.flipped=!musicContainer.flipped;
                break;
            case "forward":
                audioPlayer.muted = !audioPlayer.muted;
                console.log("The player is now " + audioPlayer.muted ? "" : "un-" + "muted");
                break;
            case "zoom_out":
                audioPlayer.volume = audioPlayer.volume>0.0 ? audioPlayer.volume-0.1 : 0;
                console.log("Volume down bebeh !!!");
                break;
            case "zoom_in":
                audioPlayer.volume = audioPlayer.volume<1.0 ? audioPlayer.volume+0.1 : 1.0;
                console.log("Volume up bebeh !!!");
                break;
        }
    }
}
