import QtQuick 1.1
import "../shared"
import "../"

Item
{
    id: musicDetailView
    width: musicContainer.width
    height: musicContainer.height
    MyEffects{
        id: bgEffects
        anchors.fill: parent;
        count:30
        emissionRate: 15
        emissionVariance: 50
        opacity: 0.5
        lifeSpan: 3000
        lifeSpanDeviation: 6000
        source: "../effects/white.png";
        xvar: 5
        yvar: 90
        velocity: 20
    }

    Rectangle{
        id: background
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#cdc7f5"
            }

            GradientStop {
                position: 1
                color: "#48536d"
            }
        }
        anchors.fill: parent
        opacity: 0.3
    }
    MouseArea{
        anchors.fill: parent
        onClicked: {
            flipped = !flipped;
            rotation.angle = 0;
            console.log(flipped);
        }
    }
    Text{
        id: nowPlayingText
        text: "<b>Now playing : </b> <i><br/>" + musicContainer.filePlaying + "</i>";
        color: audioPlayer.muted ? "grey" : "black"
        font.pixelSize: 30
        anchors.centerIn: parent
    }
    Text{
        text: "Time left : " + audioPlayer.remainingTime;
        anchors.bottom: parent.bottom
//        anchors.top: nowPlayingText.bottom
//        anchors.topMargin: 30
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 20;
        font.family: "Helvetica"
    }

    Rectangle{
        id: progressBar
        opacity: 0.5
        anchors.bottom: parent.bottom
        property double fitToWidth: ( musicContainer.width / (audioPlayer.duration/500) ); //this is to dynamically map the value of remaining time to the width of the app in pixels
        height:32
        radius: 1
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#191742"
            }

            GradientStop {
                position: 1
                color: "#4d7ee8"
            }
        }
        width: ( (audioPlayer.position/500) * fitToWidth )
        Behavior on width {
            PropertyAnimation{duration:500}
        }
    }
    Rectangle{
        id: volumeBar
        property double fitToHeight: parent.height/100
        height: fitToHeight*2*(audioPlayer.volume*100);
//        height: parent.height
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#b9deff"
            }

            GradientStop {
                position: 0.860
                color: audioPlayer.muted ? "black" : "#00000000"
            }
        }
        width: 32
        anchors.right: parent.right
    }
}
