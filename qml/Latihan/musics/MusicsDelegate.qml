// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Component{
    Item {
        id: drawer
        property bool isPlaying: false;
        x: 5
        height: 40
        width: parent.width
        Row {
            spacing: 10
            MouseArea
            {
                height: drawer.height
                width: parent.parent.width
                Rectangle {
                    id: contextContainer
                    anchors.fill: parent
                    radius: 7
                    gradient: Gradient {
                        GradientStop {
                            position: 0
                            color: "#6782b1"
                        }

                        GradientStop {
                            position: 0.530
                            color: "#00000000"
                        }
                    }
                    border.width: 0
                    border.color: "#000000"
                    Text {
                        text: fileName;
//                        text: artistName + " - " + songName;
                        anchors.verticalCenter: parent.verticalCenter
                        font.bold: true
                        color: "white"
                    }
                }
                onClicked:
                {
                    listView.currentIndex = index;
                    mFileClicked();
                    musicContainer.flipped = !musicContainer.flipped;
                }
            }
        }
        function mFileClicked()
        {
            if (!isPlaying)
            {
                musicContainer.filePlaying = fileName;
                audioPlayer.source = "../../../MediaFiles/Musics/" + musicContainer.filePlaying;
                audioPlayer.play();
            }
            else
                audioPlayer.pause();

            drawer.isPlaying = !drawer.isPlaying;
        }
    }

}
