import QtQuick 1.1

// Toolbar buttons for the Music application
ListModel {
    ListElement {
        buttonText:"PREV"
        event: "track_prev"
        iconImage: "./icons/previous.png"
    }
    ListElement {
        buttonText: "PAUSE"
        event: "track_pause"
        iconImage: "./icons/play.png"
    }
    ListElement {
        buttonText: "NEXT"
        event: "track_next"
        iconImage: "./icons/next.png"
    }
}
