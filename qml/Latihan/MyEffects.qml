// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt.labs.particles 1.0
//import "shared/storage.js"

Particles {
    id:effects

    property int xvar: 20
    property int yvar: 30
    property string pColor : "white";

    lifeSpan: 700
    lifeSpanDeviation: 2000
    emissionRate: 0
    count: 20
    angle: 0
    angleDeviation: 360
    velocity: 60
    velocityDeviation: 50
    smooth:true
    ParticleMotionWander {
        xvariance: xvar
        yvariance: yvar
        pace: 60
    }

    source: "./effects/" + pColor + ".png";
//    function randomParticles(){
//        //Array of particle colors
//        var images = ["blue", "green", "white", "orange", "purple"]
//        // Get random index of the array element
//        var idx = Math.floor((Math.random() * 100)) % images.length
//        // Return the relative image file path
//        return ("./effects/"+images[idx]+".png")
//    }
}
