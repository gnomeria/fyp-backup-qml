import QtQuick 1.1

Item{
    height: 40
    width: 100
    MouseArea{
        anchors.fill: parent
        Image{
            id: togImage
            height: parent.height
            width: parent.width
            property bool togState: true
            property string imgPrefix: "../images/switch-";
            source: togState ? (imgPrefix+"on.png") : (imgPrefix+"off.png")
//            scale: 0.7
//            anchors.verticalCenter: parent.verticalCenter
            smooth: true
        }
        onClicked: {
            togImage.togState = !togImage.togState;
            console.log(togImage.togState);
        }
    }
}
