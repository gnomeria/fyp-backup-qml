// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt.labs.shaders 1.0
import "../shared/storage.js" as Storage

Item{
    id: settingsRoot
    anchors.fill: parent
    property bool _particleBgSetting: (Storage.getSetting("particleEnabled"));
    signal dbChanged;
    Rectangle {
        id: background
        anchors.fill: parent
        color: "Black"
        opacity: 0.5

    }
    Component.onCompleted: {
        Storage.initialize();
        Storage.setSetting("particleEnabled", _particleBgSetting); //setting the particle effects persistent data
    }
    Text{
        id: particleBgText
        text: "Particle background setting : "
        color: "white"
        anchors.top: settingsRoot.top;
        anchors.left: parent.left
        anchors.leftMargin: 30
    }

    Image{
        id: particleBgToggle
        height: 30
        anchors.left: text1.right
        anchors.leftMargin: 10
        source: _particleBgSetting ? "./images/switch-on.png" : "./images/switch-off.png"
        fillMode: Image.PreserveAspectFit
        MouseArea{
            anchors.fill: parent;
            onClicked: toggle()
        }
    }
    //----------------------- Row 2 ---------------------------------
    Text{
        id: particleColorText
        text: "Change effects color : "
        color: "white"
        anchors.left: parent.left
        anchors.leftMargin: 30
        anchors.top: particleBgText.bottom
        anchors.topMargin: 40
    }
    ColorPicker{id: combobox; anchors.left: particleColorText.right}
//    Below is save button
    MouseArea{
        height: 54
        width: 100
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 50
        anchors.horizontalCenter: parent.horizontalCenter

        Rectangle{
            id: saveButton
            height: parent.height
            width: parent.width
            color: "black"
            radius: 3

            Text{
                text: "Save settings"
                color: "white"
                anchors.fill: setingsRoot
            }
        }
    }

    function toggle()
    {
        var _off = "../Latihan/images/switch-off.png";
        var _on = "../Latihan/images/switch-on.png";
        particleBgToggle.source = _particleBgSetting ? _on:_off;
        Storage.setSetting("particleEnabled", _particleBgSetting);
//        dbChanged();
        _particleBgSetting = !_particleBgSetting;
    }
    function saveDB()
    {
        Storage.setSetting("particleEnabled", tog1);
        Storage.setSetting("effColor", "blue");
        dbChanged();
    }
}
