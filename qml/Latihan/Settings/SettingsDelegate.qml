import QtQuick 1.1

Component{
    Item
    {
        id: drawer
        x: 20
        height: 40
        width: parent.width
        Rectangle
        {
            anchors.fill: parent
            color: "transparent"
        Row{
            spacing: 20
            Text{
                anchors.verticalCenter: parent.verticalCenter
                text: "<b>"+settingName+"</b>";
                color: "white"
                font.pointSize: 15
            }
            Loader {
                source: _optionModel
                anchors.verticalCenter: parent.verticalCenter
            }

        }
        }


    }
}
