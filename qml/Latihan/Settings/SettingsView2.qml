import QtQuick 1.1

Item{
    id: settingsViewRoot
    anchors.fill: parent
    Rectangle{
        id: background
        color: "black"
        opacity: 0.5
        anchors.fill: parent
}
        ListView{
            id: settingsListView
            anchors.top: parent.top
            anchors.topMargin: 30
            height: parent.height
            width: parent.width
            //        anchors.fill: parent

            delegate: SettingsDelegate{}
            model: SettingsModel{}
            //        boundsBehavior: Flickable.DragAndOvershootBounds
            //        snapMode: ListView.SnapToItem
            spacing: 5
        }
//    }
}
