import QtQuick 1.1

ListModel{
    ListElement{
        settingId       : "_particleBgSetting"
        settingName     : "Particle background"
        _optionModel    : "ToggleButton.qml"
    }
    ListElement{
        settingId       : "_particleColorSetting"
        settingName     : "Particle color "
        _optionModel    : "ColorPicker.qml"
    }
//    ListElement{
//        settingId       : "_bgSetting"
//        settingName     : "Choose application background "
//        _optionModel    : "ChooseBackground.qml"
//    }
}
