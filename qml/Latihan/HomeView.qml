import QtQuick 1.1
import "./qticcontent"
Item {
    id: mainView
//    gearSetter:
    anchors.fill: parent
    DistanceControl{
        id: disCont
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        scale: 0.8
        smooth: true
    }
    onHeightChanged: disCont.scale=parent.height/318-0.2
//    onHeightChanged: console.log(parent.height);
    Instrument{
        anchors.verticalCenter: parent.verticalCenter;
        anchors.horizontalCenter: parent.horizontalCenter;
//        visible:false
    }

    GearBar{
        id: gearbar
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter;
    }
//    StatusBar{
//        anchors.right: parent.right
//        anchors.bottom: parent.bottom
//    }

}



