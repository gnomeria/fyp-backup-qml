import QtQuick 1.1

MouseArea {
    property string iName: "home"

    id: _icons
    width: 100
    height: 100
    function _iconNormal() { _icon.source = "./icons/"+iName+".png"}
    function _iconHover() { _icon.source = "./icons/"+iName+"_hover.png" }

    Image{ id:_icon; width: 100; height: 100; smooth: true;source:"./icons/"+iName+".png"}
    hoverEnabled:true
    //Glows when hover, and return to normal when the mouse cursor leave the topbar buttons (icons)
    onEntered: _iconHover()
    onExited: _iconNormal()
    onClicked: {
        //Creates a particle effects
        //Map the particles effects on a center of topbar icons
        eff.anchors.verticalCenter=_icons.verticalCenter;
        eff.anchors.horizontalCenter=_icons.horizontalCenter;
        eff.burst(7,50);
//        eff.source=eff.randomParticles();
        container.changeState(iName+"State")
    }

}
