// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import PerformanceAnalysis 1.0
import "./qticcontent"
import "./botbar"
import "./shared/storage.js" as Storage

Item {
    id: root
    width: 800
    height: 480
    property bool fullscreen: false;
    property string _bgfile: "./bg3.jpg";
    property bool debugging: false;
    property string currentApp: "home";
    property bool botbarShown: false;

    signal fscreen;

    Component.onCompleted: Storage.initialize();

    Image{  //Background image
        id: background
        anchors.fill: root
//        fillMode: Image.Tile;   //Fill all spaces by repeating the background sprites
        fillMode: Image.Stretch;
        source:_bgfile;
        cache: false
    }

    MyEffects{
        id: eff
        anchors.fill: parent;
        count:20
        emissionRate: 15
        emissionVariance: 50
        opacity: 0.5
        lifeSpan: 3000
        lifeSpanDeviation: 6000
        source: Storage.getSetting("effColor");
        xvar: 5
        yvar: 90
        velocity: 20
        visible: Storage.getSetting("particleEnabled");
    }
//    TopBar{id: topbar; z:1; anchors.top: root.top}
    TopBar{id: topbar; z:1;}

    HomeClock{
        id: clock
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        textColor: "white"
        z:1
    }

    NewBotBar{
        id: botbar
        z:1
        anchors.left: root.left
        anchors.leftMargin: 30
        anchors.bottom: root.bottom;
        anchors.right: clock.left;
        width: parent.width-clock.width;
        onBtnClicked: {
            console.log("FROM MAIN : " + event);
            container.handleToolbarEvent(event);
        }
        opacity: 0

        Behavior on opacity
        {
            SmoothedAnimation {duration: 1000}
        }
    }

    Container{
            id: container   //Container acts as a loader
            objectName: "container"
            anchors.top: topbar.bottom
            width: parent.width
            //the height determined by is the bottom bar is shown or not
            height: parent.height-(topbar.height + (botbarShown ? botbar.height:0))
            z: 0;


            }

    Connections{
        target: container
        onShowBotbarSig: {
            hideBotbar();
        }
        onInitDb: {
            databaseChanged();
        }
    }

    MouseArea{  //fullscreen button
        id: maFullscreen
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        width: 80
        height: 62
        hoverEnabled: true
        Image{
            id: fsButton
            anchors.fill: parent;
            source: "icons/fullscreen.png"
            opacity: 0

            Behavior on opacity
            {
                NumberAnimation { easing.type: Easing.OutQuad;duration: 1000}
            }
        }
        onClicked: goFullscreen();
        onEntered: fsButton.opacity=1
        onExited: fsButton.opacity=0
    }

//------------------------------------------------------------------------------------------------------
    PerformanceMeter {
        id: performanceMeter
        anchors.right: parent.right
        width: 150
        height: 62
        visible: parent.debugging
    }

    //---------------------------------------------------------------------------------------------------
    states: [
        State {
            name: "fullscreen"
            AnchorChanges {
                target: container
                anchors.bottom: root.bottom
                anchors.top: root.top
            }

            PropertyChanges {
                target: topbar
                y: -100
            }
            AnchorChanges {
                target: topbar
                anchors.top: undefined
            }
        },
        State {
            name: "normal"
            AnchorChanges{
                target: botbar
                anchors.bottom: root.bottom
            }
            AnchorChanges {
                target: container
                anchors.top: topbar.bottom
                anchors.bottom: botbar.top
//                anchors.bottom: botbar.top
            }

        }
    ]
    //--------------------------------------------- END OF STATES -----------------------------------------------------
    transitions: Transition
    {
        SequentialAnimation
        {
            AnchorAnimation {duration: 500; easing.type: Easing.OutSine}
            PropertyAnimation{duration: 500; easing.type: Easing.OutSine}
        }
    }

    function goFullscreen()
    {
        if (!fullscreen)
        {
            root.state = "fullscreen";
            fullscreen = true;
        }
        else
        {
            root.state = "normal";
            fullscreen=false;
        }
    }

    function hideBotbar()
    {
        if (!botbarShown)
        {
//            botbar.anchors.bottom = root.bottom;
            bbToShown.start();
//            botbar.opacity = 1; //to display the bottom bar
        }
        else
            bbToHide.start();
//            botbar.opacity = 0; //to hides the bottom bar

        console.log(botbarShown);
        botbarShown = !botbarShown;
    }
//---------------------------------------------------- HANDLING TOOLBAR ACTION -------------------------------------------------
    function setBotbarModel(model)
    {
        botbar.children.model = model;
    }
    SequentialAnimation{
        id: bbToShown
        AnchorAnimation {duration: 500}
        PropertyAnimation {target: botbar; property: "opacity"; easing.type: Easing.InCirc; to: 1; duration: 500}
    }
    SequentialAnimation{
        id: bbToHide
        AnchorAnimation {duration: 500}
        PropertyAnimation {target: botbar; property: "opacity"; to: 0; duration: 500; easing.type: Easing.OutCirc}
    }

    function databaseChanged()
    {
        eff.pColor = Storage.getSetting("effColor");
        eff.visible = Storage.getSetting("particleEnabled");
    }

}
