#ifndef FULLSCREENCONTROL_H
#define FULLSCREENCONTROL_H

#include <QObject>
#include <QDebug>
#include "qmlapplicationviewer.h"

class FullScreenControl : public QObject
{
    Q_OBJECT
    public:
    FullScreenControl(QmlApplicationViewer* viewer)
    {
        this->viewer=viewer;
    }

    public slots:
        void videoDoubleClicked()
        {
            if ( isFullscreen )
            {
                qDebug() << isFullscreen;
                viewer->showNormal ();
                isFullscreen = !isFullscreen;
            }
            else
            {
                qDebug() << isFullscreen;
                viewer->showFullScreen ();
                isFullscreen = !isFullscreen;
            }
        }

//    signals:
//        void fscreenClicked();

    private:
        bool isFullscreen;
        QmlApplicationViewer* viewer;
        QObject* rootObject;
};

#endif // FULLSCREENCONTROL_H

