//#include <taglib/fileref.h>
//#include <taglib/tag.h>
//#include <taglib/mpegfile.h>
//#include <taglib/id3v2tag.h>
//#include <taglib/attachedpictureframe.h>
//#include <QImage>
//#include <QDebug>
//#include <QDir>
//#include "musicmodel.h"

//MusicModel::MusicModel(const QString &path)
//    : m_title("Unkown Song"), m_artist("Unkown Artist"), m_album("Unkown Album"),
//      m_genre(""), m_length("00:00"), m_path(path), m_selected(false)
//{
//    if(!QDir::home().cd(".music-player"))
//        QDir::home().mkdir(".music-player");

//    QFileInfo file(m_path);
//    TagLib::FileRef f(path.toStdString().c_str());

//    if(!f.isNull() && f.tag()) {
//        TagLib::Tag *tag = f.tag();

//        if(!tag->title().isEmpty())
//            m_title = QString::fromUtf8(tag->title().toCString(true));
//        if(!tag->genre().isEmpty())
//            m_genre = QString::fromUtf8(tag->genre().toCString(true));

//        m_year = tag->year();
//        m_track = tag->track();

//        m_imageSource = "../images/default.png";

//        if(!tag->artist().isEmpty() && !tag->album().isEmpty()){
//            m_album = QString::fromUtf8(tag->album().toCString(true));
//            m_artist = QString::fromUtf8(tag->artist().toCString(true));

//            if(file.suffix() == "mp3")
//                loadMp3AlbumCover();
//        }

//        if(f.audioProperties()){
//            TagLib::AudioProperties *properties = f.audioProperties();

//            int seconds = properties->length() % 60;
//            QString secondsStr;
//            secondsStr.sprintf("%02i", seconds);
//            int minutes = (properties->length() - seconds) / 60;
//            m_length = QString::number(minutes) + ":" + secondsStr;
//        }
//    }
//}

//void MusicModel::loadMp3AlbumCover()
//{
//    QString imageDir = QDir::home().path() + "/.music-player/";
//    QString imagePath = imageDir + m_artist + "-" + m_album + ".jpeg";
//    imagePath.remove(' ');
//    QFile imageFile(imagePath);
//    TagLib::MPEG::File mp3(m_path.toStdString().c_str());

//    if(mp3.ID3v2Tag() && !imageFile.exists()){
//        QImage image;
//        TagLib::ID3v2::FrameList l = mp3.ID3v2Tag()->frameListMap()["APIC"];

//        if(!l.isEmpty()){
//            TagLib::ID3v2::AttachedPictureFrame *pic = static_cast<TagLib::ID3v2::AttachedPictureFrame *>(l.front());
//            image.loadFromData((const uchar *) pic->picture().data(), pic->picture().size());

//            if(!image.isNull()) {
//                image = image.scaled(300, 300);
//                bool trySaving = image.save(imagePath, 0, 100);
//                if(trySaving) {
//                    qDebug() << "-- Img Saved at " << imagePath;
//                    m_imageSource = imagePath;
//                }
//            }
//        }
//    }
//    else if(imageFile.exists()) {
//        m_imageSource = imagePath;
//    }
//}

//QString MusicModel::title() const
//{
//    return m_title;
//}

//QString MusicModel::artist() const
//{
//    return m_artist;
//}

//QString MusicModel::album() const
//{
//    return m_album;
//}

//int MusicModel::year() const
//{
//    return m_year;
//}

//int MusicModel::track() const
//{
//    return m_track;
//}

//QString MusicModel::genre() const
//{
//    return m_genre;
//}

//QString MusicModel::length() const
//{
//    return m_length;
//}

//QString MusicModel::path() const
//{
//    return m_path;
//}

//QString MusicModel::imageSource() const
//{
//    return m_imageSource;
//}

//bool MusicModel::selected() const
//{
//    return m_selected;
//}

//void MusicModel::setSelected(bool selected)
//{
//    m_selected = selected;
//}

//bool MusicModel::operator<(const MusicModel &music) const
//{
//    if(m_artist < music.artist())
//        return true;
//    else if(m_artist == music.artist())
//        return m_title <= music.title();
//    else
//        return false;
//}


//MusicModelModel::MusicModelModel(QObject *parent)
//    : QAbstractListModel(parent)
//{
//    QHash<int, QByteArray> roles;
//    roles[TitleRole] = "title";
//    roles[ArtistRole] = "artist";
//    roles[AlbumRole] = "album";
//    roles[YearRole] = "year";
//    roles[TrackRole] = "track";
//    roles[GenreRole] = "genre";
//    roles[LengthRole] = "length";
//    roles[PathRole] = "path";
//    roles[ImageSourceRole] = "imageSource";
//    roles[SelectedRole] = "selected";
//    setRoleNames(roles);
//    m_currentIndex = 0;
//}

//int MusicModelModel::rowCount(const QModelIndex &parent) const
//{
//    return m_files.count();
//}

//QVariant MusicModelModel::data(const QModelIndex &index, int role) const
//{
//    if (index.row() < 0 || index.row() > m_files.count())
//        return QVariant();

//    const MusicModel &file = m_files[index.row()];
//    if (role == TitleRole)
//        return file.title();
//    else if (role == ArtistRole)
//        return file.artist();
//    else if (role == AlbumRole)
//        return file.album();
//    else if (role == YearRole)
//        return file.year();
//    else if (role == TrackRole)
//        return file.track();
//    else if (role == GenreRole)
//        return file.genre();
//    else if (role == LengthRole)
//        return file.length();
//    else if (role == PathRole)
//        return file.path();
//    else if (role == ImageSourceRole)
//        return file.imageSource();
//    else if (role == SelectedRole)
//        return file.selected();

//    return QVariant();
//}

//void MusicModelModel::addMusicModel(const MusicModel &MusicModel)
//{
//    beginInsertRows(QModelIndex(), m_files.count(), m_files.count());
//    m_files << MusicModel;
//    endInsertRows();
//}

//MusicModel MusicModelModel::dataAt(int index) const
//{
//    return m_files.at(index);
//}

//MusicModel MusicModelModel::currentSongData() const
//{
//    return m_files.at(m_currentIndex);
//}

//void MusicModelModel::remove(int index)
//{
//    qDebug() << Q_FUNC_INFO << " index: " << index << "  current: " << m_currentIndex;

//    if(index < m_currentIndex || m_currentIndex == m_files.count()-1)
//        decrementIndex();

//    beginRemoveRows(QModelIndex(), index, index);
//    m_files.removeAt(index);
//    endRemoveRows();

//    qDebug() << Q_FUNC_INFO << " current: " << m_currentIndex;
//}

//void MusicModelModel::replace(int i, MusicModel music)
//{
//    m_files.replace(i, music);
//    emit dataChanged(index(i), index(i));
//}

//void MusicModelModel::sort()
//{
//    emit layoutAboutToBeChanged();
//    qSort(m_files.begin(), m_files.end());
//    emit layoutChanged();
//}

//int MusicModelModel::currentIndex() const
//{
//    return m_currentIndex;
//}

//void MusicModelModel::setCurrentIndex(int index)
//{
//    m_currentIndex = index;
//}

//void MusicModelModel::incrementIndex()
//{
//    if(m_currentIndex < m_files.count()-1)
//        m_currentIndex++;
//}

//void MusicModelModel::decrementIndex()
//{
//    if(m_currentIndex > 0)
//        m_currentIndex--;
//}

//bool MusicModelModel::ended() const
//{
//    return m_currentIndex == m_files.count()-1 || empty();
//}

//bool MusicModelModel::empty() const
//{
//    return m_files.empty();
//}
