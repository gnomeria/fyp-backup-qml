#ifndef MUSICMODEL_H
#define MUSICMODEL_H

#include <QObject>
#include <QAbstractListModel>

class MusicModel
{
public:
    MusicModel(const QString &path);

    QString title() const;
    QString artist() const;
    QString length() const;
    QString path() const;

    bool selected() const;
    void setSelected(bool selected);

    bool operator<(const MusicModel &music) const;

private:
    QString m_title;
    QString m_artist;
    QString m_length;
    QString m_path;
    QString m_imageSource;
    bool m_selected;
};

//class MusicModelModel : public QAbstractListModel
//{
//    Q_OBJECT
//public:
//    enum MusicModelRoles {
//        TitleRole = Qt::UserRole + 1,
//        ArtistRole,
//        TrackRole,
//        LengthRole,
//        PathRole,
//        ImageSourceRole,
//        SelectedRole
//    };

//    MusicModelModel(QObject *parent = 0);

//    int rowCount(const QModelIndex &parent = QModelIndex()) const;
//    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

//    void addMusicModel(const MusicModel &MusicModel);
//    void remove(int index);
//    void replace(int i, MusicModel music);
//    void sort();

//    int currentIndex() const;
//    void setCurrentIndex(int index);
//    void incrementIndex();
//    void decrementIndex();
//    bool ended() const;
//    bool empty() const;
//    MusicModel dataAt(int index) const;
//    MusicModel currentSongData() const;

//private:
//    int m_currentIndex;
//    QList<MusicModel> m_files;
//};


#endif // MUSICMODEL_H
